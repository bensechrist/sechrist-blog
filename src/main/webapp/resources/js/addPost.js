$('#postTags').selectize({
	valueField: 'name',
    labelField: 'name',
    searchField: 'name',
	delimiter: ',',
	persist: false,
	highlight: true,
	maxOption: 5,
	create: true,
	openOnFocus: true,
	hideSelected: true,
	render: {
        option: function(item, escape) {
        	console.log(item.name);
        	return "<div>" + escape(item.name) + "</div>";
        }
    },
	load: function(query, callback) {
		if (!query.length) return callback();
		$.ajax({
			url: CONTEXT_URL + '/api/tag/search/' + encodeURIComponent(query),
			type: 'GET',
			error: function() {
				console.error("addPost.js: Error: tag search");
				callback();
			},
			success: function(res) {
				callback(res);
			}
		});
	}
});

// Text Editor Setup

$('#postText').wysiwyg();

$('.dropdown-menu').find('input').click(function(e) {
	e.stopPropagation();
});

$('#datePosted').datepicker();

function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            
            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}

function dropzoneAccept(file, done) {
	console.log(file.name);
	if ((file.name.substr(file.name.length - 4) != ".jpg")
			&& (file.name.substr(file.name.length - 4) != ".JPG")
			&& (file.name.substr(file.name.length - 5) != ".jpeg")
			&& (file.name.substr(file.name.length - 5) != ".JPEG")
			&& (file.name.substr(file.name.length - 4) != ".gif")
			&& (file.name.substr(file.name.length - 4) != ".GIF")) {
		done("File must be jpeg or gif format");
	}
	else { done(); }
}

function dropzoneSuccess(file, response) {
	$('#postText').focus();
	pasteHtmlAtCaret(response.html + "<br />");
	if ($('#imageNames').val() != "")
		$('#imageNames').val($('#imageNames').val() + "," + response.filename);
	else
		$('#imageNames').val(response.filename);
}

function dropzoneProgress(file, progress) {
	$('.progress').css("display", "inherit");
	$('.progress-bar').css("width", progress + "%");
	$('.progress-bar').text(Math.round(progress) + "%");
}

function dropzoneError(file, message) {
	alert(message);
}

Dropzone.autoDiscover = false;

$('#postText').dropzone({ 
	url: CONTEXT_URL + "/api/image/upload",
	maxFilesize: 10,
	clickable: false,
	previewsContainer: "#preview",
	accept: function(file, done) {
		dropzoneAccept(file, done);
	},
	init: function() {
		this.on("success", function(file, response) {
			dropzoneSuccess(file, response);
		});
		this.on("uploadprogress", function(file, progress) {
			dropzoneProgress(file, progress);
		});
		this.on("error", function(file, message) {
			dropzoneError(file, message);
		});
	}
});

$('#uploadBtn').dropzone({ 
	url: CONTEXT_URL + "/api/image/upload",
	maxFilesize: 10,
	previewsContainer: "#preview",
	dictDefaultMessage: "Upload",
	accept: function(file, done) {
		dropzoneAccept(file, done);
	},
	init: function() {
		this.on("success", function(file, response) {
			dropzoneSuccess(file, response);
		});
		this.on("uploadprogress", function(file, progress) {
			dropzoneProgress(file, progress);
		});
		this.on("error", function(file, message) {
			dropzoneError(file, message);
		});
	}
});

$('#uploadBtn').click(function() {
	return false;
});

$('#publishBtn').click(function() {
	var x = confirm("Are you sure you want to publish this Post?");
	if (x == false)
		return false;
});

$('#addLink').click(function() {
	var href = $('#linkHref').val();
	var title = $('#linkTitle').val();
	
	if (!href) {
		alert("Enter a URL");
		return false;
	}
	if (!title) {
		alert("Enter a title");
		return false;
	}
	
	if (!href.match(/^http([s]?):\/\/.*/)) {
		$('#linkHref').val('http://' + href);
		href = $('#linkHref').val();
	}
	
	$('#postText').focus();
	pasteHtmlAtCaret("<a href=\"" + href + "\" target=\"_blank\">" + title + "</a>");
	$('#linkHref, #linkTitle').val("");
	return false;
});

$('form').submit(function() {
	$('#postTextContent').val($('#postText').html());
});

$(document).ready(function() {
	$('#postText').html($('#postTextContent').val());
	
	window.onbeforeunload = function(e) {
		e = e || window.event;
		
		var activeElementId = $(e.target.activeElement).attr("id");
		if ((activeElementId == "publishBtn") || (activeElementId == "saveBtn")) {
			console.log($(e.target.activeElement).attr("id"));
			window.onbeforeunload = null;
			return null;
		}
		
		var message = "Navigigating away from this page will lose your work. Save before leaving!";
		
		if (e) {
			e.returnValue = message;
		}
		
		return message;
	};
});
