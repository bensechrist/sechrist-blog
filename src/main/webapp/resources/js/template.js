$( ".searchBar" ).autocomplete({
	minLength: 2,
	source: function( request, response ) {
		$.getJSON( CONTEXT_URL + "/api/posts/search/" + request.term, 
				response );
	},
	focus: function(event, ui) {
		$(".searchBar").val(ui.item.label);
        return false;
	},
	response: function(event, ui) {
		var resultArray = ui.content.slice(0);
		while(ui.content.length > 0) {
			ui.content.pop();
		}
		for(var i=0; i<resultArray.length; i++) {
			var monthNames = [ "January", "February", "March", "April", "May", "June",
               "July", "August", "September", "October", "November", "December" ];
			var date = new Date(resultArray[i].datePosted);
			ui.content.push({
				label: resultArray[i].title, 
				value: resultArray[i].id,
				date: (date.getDate()+1) + "." + monthNames[date.getMonth()] + "." + date.getFullYear()
			});
		}
		return false;
	},
	select: function(event, ui) {
		window.location = CONTEXT_URL + "/posts/" + ui.item.label;
		return false;
	}
})
.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	return $( "<li>" )
	.append( "<a>" + item.label + "<br>" + item.date + "</a>" )
	.appendTo( ul );
};

$('#commitId').tooltip();

$('#searchForm').submit(function(event) {
	window.location = CONTEXT_URL + "/search/" + $('.searchBar').val();
	event.preventDefault();
});