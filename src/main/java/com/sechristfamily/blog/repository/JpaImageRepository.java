package com.sechristfamily.blog.repository;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.domain.ImageEntity;

@ApplicationScoped
public class JpaImageRepository implements ImageRepository {

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Override
	public ImageEntity newImage(String filename) {
		ImageEntity image = new ImageEntity();
		image.setFileName(filename);
		entityManager.persist(image);
		return image;
	}
	
	@Override
	public ImageEntity get(String filename) {
		try {
			return entityManager
				.createNamedQuery("findImageByName", ImageEntity.class)
				.setParameter("fileName", filename)
				.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public void delete(Image image) {
		try {
			ImageEntity imageEntity = entityManager
				.createNamedQuery("findImageByName", ImageEntity.class)
				.setParameter("fileName", image.getFileName())
				.getSingleResult();
			entityManager.remove(imageEntity);
		} catch (NoResultException e) {
			// Nothing
		}
	}

	@Override
	public List<Image> random(int num) {
		List<ImageEntity> imageEntities = entityManager
				.createQuery("SELECT i FROM ImageEntity i JOIN i.post p WHERE p.published = true ORDER BY RAND()", ImageEntity.class)
				.setMaxResults(num)
				.getResultList();
		List<Image> result = new ArrayList<>();
		for (ImageEntity i : imageEntities)
			result.add(i);
		return result;
	}

	@Override
	public boolean exists(String filename) {
		ImageEntity realImage = get(filename);
		if (realImage != null)
			return true;
		else
			return false;
	}

}
