package com.sechristfamily.blog.repository;

import java.util.List;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.domain.TagEntity;

public interface TagRepository {

	TagEntity getTag(String name);
	
	TagEntity newTag(String name);
	
	Boolean tagExists(String name);
	
	List<Tag> search(String query);
	
	void delete(String name);
}
