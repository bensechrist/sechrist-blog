package com.sechristfamily.blog.repository;

import java.util.Date;
import java.util.List;

import com.sechristfamily.blog.Post;

public interface PostRepository {
	
	void saveForLater(Post post);

	void add(Post post);
	
	void updateSave(Post post);
	
	void publish(Post post);
	
	List<Post> getAll(int index);
	
	List<Post> getPublished(int index);
	
	Post get(Long id);
	
	Post get(String title);
	
	void delete(Long id);
	
	void update(Post post);
	
	List<Post> getAllByTag(String name);
	
	List<Post> getPublishedByTag(String name);
	
	List<Post> search(String query);
	
	List<Post> searchAll(String query);

	int numberOfPages();
	
	int numberOfPages(String query);

	String getNewerPostTitle(Date datePosted);

	String getOlderPostTitle(Date datePosted);

	String getNewerPublishedPostTitle(Date datePosted);

	String getOlderPublishedPostTitle(Date datePosted);

	Date getLastUpdatedDate();

}
