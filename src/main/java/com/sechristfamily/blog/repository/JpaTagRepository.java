package com.sechristfamily.blog.repository;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.domain.TagEntity;

@ApplicationScoped
public class JpaTagRepository implements TagRepository {

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Override
	public TagEntity getTag(String name) {
		try {
			return entityManager
				.createNamedQuery("findTagByName", TagEntity.class)
				.setParameter("tag", name)
				.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public TagEntity newTag(String name) {
		TagEntity tag = new TagEntity();
		tag.setName(name);
		entityManager.persist(tag);
		return tag;
	}

	@Override
	public Boolean tagExists(String name) {
		TagEntity tag = getTag(name);
		return (tag != null);
	}

	@Override
	public List<Tag> search(String query) {
		List<Tag> tags = new ArrayList<Tag>();
		List<TagEntity> entityTags = entityManager
			    .createQuery("SELECT t FROM TagEntity t WHERE t.name LIKE :query", TagEntity.class)
			    .setParameter("query", query + "%")
			    .getResultList();
		tags.addAll(entityTags);
		return tags;
	}

	@Override
	public void delete(String name) {
		try {
			TagEntity tag = entityManager
				.createNamedQuery("findTagByName", TagEntity.class)
				.setParameter("tag", name)
				.getSingleResult();
			entityManager.remove(tag);
		} catch (NoResultException e) {
			// Nothing
		}
	}

}
