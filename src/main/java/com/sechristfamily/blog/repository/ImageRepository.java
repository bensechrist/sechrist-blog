package com.sechristfamily.blog.repository;

import java.util.List;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.domain.ImageEntity;

public interface ImageRepository {
	
	ImageEntity newImage(String filename);

	ImageEntity get(String filename);
	
	void delete(Image image);
	
	List<Image> random(int num);
	
	boolean exists(String filename);
}
