package com.sechristfamily.blog.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.Validate;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.builders.PostEntityConverter;
import com.sechristfamily.blog.domain.ImageEntity;
import com.sechristfamily.blog.domain.PostEntity;
import com.sechristfamily.blog.domain.TagEntity;
import com.sechristfamily.blog.models.ImageModel;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.models.TagModel;
import com.sechristfamily.blog.security.UserService;

@ApplicationScoped
public class JpaPostRepository implements PostRepository, PostEntityConverter {

	private static final int MAX_RESULTS_PER_PAGE = 10;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@Inject
	protected UserService userBean;

	@Inject
	protected TagRepository tagRepository;
	
	@Inject
	protected ImageRepository imageRepository;
	
	@Override
	public void saveForLater(Post p) {
		Validate.isTrue(p instanceof PostEntity);
		PostEntity post = (PostEntity) p;
		post.setPublished(false);
		entityManager.persist(post);
		entityManager.flush();
	}

	@Override
	public void add(Post p) {
		Validate.isTrue(p instanceof PostEntity);
		PostEntity post = (PostEntity) p;
		post.setPublished(true);
		entityManager.persist(post);
		entityManager.flush();
	}

	@Override
	public List<Post> getAll(int index) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
				.createNamedQuery("getAllPosts", PostEntity.class)
				.setFirstResult((index-1)*MAX_RESULTS_PER_PAGE)
				.setMaxResults(MAX_RESULTS_PER_PAGE)
				.getResultList();
		posts.addAll(entityPosts);
		return posts;
	}
	
	@Override
	public List<Post> getPublished(int index) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
				.createNamedQuery("getPublishedPosts", PostEntity.class)
				.setFirstResult((index-1)*MAX_RESULTS_PER_PAGE)
				.setMaxResults(MAX_RESULTS_PER_PAGE)
				.getResultList();
		posts.addAll(entityPosts);
		return posts;
	}

	@Override
	public Post get(Long id) {
		PostEntity post = entityManager.find(PostEntity.class, id);
		return post;
	}
	
	@Override
	public Post get(String title) {
		title = title.replaceAll("\\.", "%");
		System.out.println("========================================= Searching for " + title);
		List<PostEntity> posts = entityManager.createNamedQuery("getPostByTitle", PostEntity.class)
				.setParameter("title", title)
				.getResultList();
		return posts.get(0);
	}

	@Override
	public void delete(Long id) {
		PostEntity post = entityManager.find(PostEntity.class, id);
		entityManager.remove(post);
	}

	@Override
	public void update(Post post) {
		Validate.isTrue(post instanceof PostEntity);
		entityManager.merge(post);
	}
	
	@Override
	public void updateSave(Post post) {
		Validate.isTrue(post instanceof PostEntity);
		entityManager.merge(post);
	}

	@Override
	public void publish(Post post) {
		Validate.isTrue(post instanceof PostEntity);
		PostEntity oldPost = entityManager.find(PostEntity.class, post.getId());
		oldPost.setPublished(true);
	}
	
	@Override
	public List<Post> getAllByTag(String name) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
		    .createNamedQuery("getAllPostsByTag", PostEntity.class)
		    .setParameter("name", name)
		    .getResultList();
		posts.addAll(entityPosts);
		return posts;
	}
	
	@Override
	public List<Post> getPublishedByTag(String name) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
		    .createNamedQuery("getPublishedPostsByTag", PostEntity.class)
		    .setParameter("name", name)
		    .getResultList();
		posts.addAll(entityPosts);
		return posts;
	}

	@Override
	public List<Post> search(String query) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
		    .createNamedQuery("searchPublishedPosts", PostEntity.class)
		    .setParameter("query", "%" + query + "%")
		    .getResultList();
		posts.addAll(entityPosts);
		return posts;
	}
	
	@Override
	public List<Post> searchAll(String query) {
		List<Post> posts = new ArrayList<Post>();
		List<PostEntity> entityPosts = entityManager
		    .createNamedQuery("searchAllPosts", PostEntity.class)
		    .setParameter("query", "%" + query + "%")
		    .getResultList();
		posts.addAll(entityPosts);
		return posts;
	}
	
	@Override
	public int numberOfPages() {
		Number number = (Number) entityManager.createNamedQuery("numberOfPosts").getSingleResult();
		int result = (int) Math.ceil(number.doubleValue()/MAX_RESULTS_PER_PAGE);
		return result;
	}
	
	@Override
	public int numberOfPages(String query) {
		Number number = (Number) entityManager.createNamedQuery("numberOfPostsWithQuery")
				.setParameter("query", "%" + query + "%")
				.getSingleResult();
		int result = (int) Math.ceil(number.doubleValue()/MAX_RESULTS_PER_PAGE);
		return result;
	}

	@Override
	public String getNewerPostTitle(Date datePosted) {
		List<PostEntity> results = entityManager.createQuery("SELECT p FROM PostEntity p "
				+ "WHERE p.datePosted > :date ORDER BY p.datePosted DESC", PostEntity.class)
				.setParameter("date", datePosted)
				.getResultList();
		if (results.isEmpty())
			return null;
		return results.get(results.size() - 1).getTitle();
	}

	@Override
	public String getOlderPostTitle(Date datePosted) {
		List<PostEntity> results = entityManager.createQuery("SELECT p FROM PostEntity p "
				+ "WHERE p.datePosted < :date ORDER BY p.datePosted DESC", PostEntity.class)
				.setParameter("date", datePosted)
				.getResultList();
		if (results.isEmpty())
			return null;
		return results.get(0).getTitle();
	}

	@Override
	public String getNewerPublishedPostTitle(Date datePosted) {
		List<PostEntity> results = entityManager.createQuery("SELECT p FROM PostEntity p "
				+ "WHERE p.datePosted > :date AND p.published = true ORDER BY p.datePosted DESC", PostEntity.class)
				.setParameter("date", datePosted)
				.getResultList();
		if (results.isEmpty())
			return null;
		return results.get(results.size() - 1).getTitle();
	}

	@Override
	public String getOlderPublishedPostTitle(Date datePosted) {
		List<PostEntity> results = entityManager.createQuery("SELECT p FROM PostEntity p "
				+ "WHERE p.datePosted < :date AND p.published = true ORDER BY p.datePosted DESC", PostEntity.class)
				.setParameter("date", datePosted)
				.getResultList();
		if (results.isEmpty())
			return null;
		return results.get(0).getTitle();
	}

	@Override
	public Post convert(PostEditModel model) {
		PostEntity post;
		if (model.getId() != null) {
			post = entityManager.find(PostEntity.class, model.getId());
		} else {
			post = new PostEntity();
		}
		post.setTitle(model.getTitle());
		post.setText(model.getText());
		post.setDatePosted(model.getDatePosted());
		post.setPublished(model.isPublished());
		Set<TagEntity> tags = new HashSet<>();
		for (TagModel t : model.getTags()) {
			TagEntity tag = tagRepository.getTag(t.getName());
			tags.add(tag);
		}
		post.setTags(tags);
		List<ImageEntity> images = new ArrayList<>();
		for (ImageModel i : model.getImages()) {
			ImageEntity image = imageRepository.get(i.getFilename());
			image.setPost(post);
			images.add(image);
		}
		post.setImages(images);
		post.setWhoEdited(userBean.getUserEmail());
		post.setLastEdited(new Date());
		return post;
	}
	
	@Override
	public Date getLastUpdatedDate() {
		return (Date) entityManager.createNativeQuery("SELECT UPDATE_TIME FROM "
				+ "information_schema.TABLES WHERE TABLE_SCHEMA = DATABASE() AND "
				+ "TABLE_NAME = 'hibernate_sequence'").getSingleResult();
	}

}
