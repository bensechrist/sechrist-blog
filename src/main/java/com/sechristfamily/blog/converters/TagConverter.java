package com.sechristfamily.blog.converters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.servlet.http.HttpServletRequest;

import com.sechristfamily.blog.models.TagModel;

@FacesConverter("com.sechristfamily.blog.TagConverter")
public class TagConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		@SuppressWarnings("unchecked")
		Set<TagModel> tags = (Set<TagModel>) arg2;
		
		List<TagModel> tagList = new ArrayList<>(tags);
		Collections.sort(tagList, new TagComparator());
		
		String result = "";

		for (TagModel tag : tagList) {
			String escapedName = "";
			try {
				escapedName = URLEncoder.encode(tag.getName(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// Do nothing
			}
			System.out.println("==================== tagConverter: " + escapedName);
			result += "<div class=\"tag inline\"><a href=\"" + request.getScheme() + "://" + request.getServerName() + ":" 
						+ request.getServerPort() + request.getContextPath() + "/tags/" +  escapedName 
						+ "\" ><span class=\"label\" >" + tag.getName() + "</span></a></div>";
		}
		
		return result;
	}

}
