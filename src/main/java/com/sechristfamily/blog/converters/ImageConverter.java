package com.sechristfamily.blog.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.sechristfamily.blog.models.ImageModel;
import com.sechristfamily.blog.service.ImageService;

@FacesConverter("com.sechristfamily.blog.ImageConverter")
public class ImageConverter implements Converter {
	
	@Inject
	private ImageService service;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		System.out.println("============= image.getAsObject: " + arg2);
		String[] array = arg2.split("\\s*,\\s*");
		List<ImageModel> images = new ArrayList<>();
		for (int i=0; i<array.length; i++) {
			if (array[i].isEmpty())
				System.out.println("============ image.getAsObject: array[" + i + "] is empty");
			else {
				images.add(service.getImage(array[i]));
			}
		}
		System.out.println("============ image.getAsObject: " + images);
		return images;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		@SuppressWarnings("unchecked")
		List<ImageModel> images = (List<ImageModel>) arg2;
		if (images.isEmpty()) {
			System.out.println("====================== Image.getAsString: No Images!");
			return null;
		}
		System.out.println("========================== Image.getAsString: " + images);
		String[] array = new String[images.size()];
		for (int i=0; i<images.size(); i++) {
			array[i] = images.get(i).getFilename();
		}
		String result = StringUtils.join(array, ',');
		System.out.println("=========================== Image.getAsString: " + result);
		return result;
	}

	
}
