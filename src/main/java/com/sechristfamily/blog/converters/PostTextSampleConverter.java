package com.sechristfamily.blog.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.jsoup.Jsoup;

@FacesConverter("com.sechristfamily.blog.PostTextSampleConverter")
public class PostTextSampleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (!(arg2 instanceof String))
			return arg2.toString();
		String text = (String) arg2;
		text = Jsoup.parse(text).text();
		if (text.length() > 250)
			return text.substring(0, 250).concat("...");
		else
			return text;
	}

}
