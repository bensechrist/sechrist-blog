package com.sechristfamily.blog.converters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.sechristfamily.blog.models.TagModel;
import com.sechristfamily.blog.service.TagService;

@FacesConverter("com.sechristfamily.blog.AddTagConverter")
public class AddTagConverter implements Converter {

	@Inject
	private TagService tagService;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		List<String> tagNames = Arrays.asList(arg2.split("\\s*,\\s*"));
		Set<TagModel> tags = new HashSet<>();
		for (String name : tagNames) {
			if (!name.isEmpty())
				tags.add(tagService.getTag(name));
		}
		return tags;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		@SuppressWarnings("unchecked")
		Set<TagModel> tags = (Set<TagModel>) arg2;
		List<String> tagNames = new ArrayList<String>();
		for (TagModel tag : tags) {
			System.out.println("=================== tag name: " + tag.getName());
			if (!tag.getName().isEmpty())
				tagNames.add(tag.getName());
			else
				System.out.println("======================= tag name is blank");
		}
		if (!tagNames.isEmpty()) {
			System.out.println("====================== TagNames is not empty");
			return StringUtils.join(tagNames, ',');
		} else {
			System.out.println("====================== TagNames is empty");
			return null;
		}
	}

}
