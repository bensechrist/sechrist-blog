package com.sechristfamily.blog.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("com.sechristfamily.blog.PostTitleUrlConverter")
public class PostTitleUrlConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ((String) arg2).replaceAll("[^A-Za-z0-9/!]+", ".");
	}

}
