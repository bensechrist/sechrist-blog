package com.sechristfamily.blog.converters;

import java.util.Comparator;

import com.sechristfamily.blog.models.TagModel;

public class TagComparator implements Comparator<TagModel> {

	@Override
	public int compare(TagModel o1, TagModel o2) {
		return (o1.getName().compareTo(o2.getName()));
	}

}
