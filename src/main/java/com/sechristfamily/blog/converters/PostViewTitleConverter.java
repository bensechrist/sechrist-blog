package com.sechristfamily.blog.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostViewModel;
import com.sechristfamily.blog.service.RetrievePostsService;

@FacesConverter("com.sechristfamily.blog.PostViewTitleConverter")
public class PostViewTitleConverter implements Converter {

	@Inject
	private RetrievePostsService service;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		PostViewModel post = service.getViewPost(arg2);
		if (post == null)
			throw new ConverterException();
		return post;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 instanceof Post)
			return ((Post) arg2).getTitle();
		return null;
	}

}
