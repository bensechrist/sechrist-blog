package com.sechristfamily.blog.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FilenameUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("/image")
public class ImageRestEndpoint {
	
	public static final String SERVER_UPLOAD_LOCATION_FOLDER = System.getProperty("user.home") + "/bloguploads/";

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> uploadFile(@Context HttpServletRequest request, MultipartFormDataInput input) {
		String fileName = "";

		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {

			try {

				// Retrieve headers, read the Content-Disposition header to obtain the original name of the file
				MultivaluedMap<String, String> headers = inputPart.getHeaders();
				fileName = parseFileName(headers);
				
				// Handle the body of that part with an InputStream
				InputStream istream = inputPart.getBody(InputStream.class,null);
				
				Map<String, String> map = new HashMap<String, String>();

				String fullPath = SERVER_UPLOAD_LOCATION_FOLDER + fileName;
				
				File f = new File(fullPath);
				Integer i = 1;
				String ext = FilenameUtils.getExtension(fileName);
				String basename = FilenameUtils.getBaseName(fileName);
				String tmpName = fileName;
				while (f.exists()) {
					tmpName = basename;
					tmpName += i.toString();
					i++;
					tmpName += "." + ext;
					f = new File(SERVER_UPLOAD_LOCATION_FOLDER + tmpName);
				}
				
				fileName = tmpName;
				fullPath = SERVER_UPLOAD_LOCATION_FOLDER + fileName;

				saveFile(istream,fullPath);
				
				map.put("html", "<img src=\"" + request.getScheme() + "://" + request.getServerName() 
						+ ":" + request.getServerPort() + request.getContextPath() + "/api/image/" + fileName + "\" />");
				map.put("filename", fileName);

				return map;
			} catch (IOException e) {
//				e.printStackTrace();
			}
		}	
		return null;
	}

	@GET
	@Path("/{fileName}")
	@Produces("image/jpeg")
	public Response getFileInJPEGFormat(@PathParam("fileName") String fileName) {
    	System.out.println("============================ File requested is : " + fileName);
     
		//Put some validations here such as invalid file name or missing file name
    	if(fileName == null || fileName.isEmpty()) {
	        ResponseBuilder response = Response.status(Status.BAD_REQUEST);
  			return response.build();
        }
     
        //Prepare a file object with file to return
        File file = new File(SERVER_UPLOAD_LOCATION_FOLDER + fileName);

    	ResponseBuilder response = Response.ok((Object) file);
    	response.header("Content-Disposition", "attachment; filename=" + fileName);
    	return response.build();
	}
	
	@DELETE
	@Path("/{fileName}")
	public Response deleteImage(@PathParam("fileName") String fileName) {
		System.out.println("============================= Delete file: " + fileName);
		
		try {
			 
    		File file = new File(SERVER_UPLOAD_LOCATION_FOLDER + fileName);
 
    		if(file.delete()) {
    			System.out.println(file.getName() + " is deleted!");
    		} else {
    			System.out.println("Delete operation is failed.");
    		}
 
    	} catch (Exception e){
    		e.printStackTrace();
    	}
		return Response.ok().build();
	}

	private String parseFileName(MultivaluedMap<String, String> headers) {
		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");

		for (String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				String[] tmp = name.split("=");

				String fileName = tmp[1].trim().replaceAll("\"","");

				return fileName;
			}
		}

		return "randomName";
	}

	private void saveFile(InputStream uploadedInputStream,
			String serverLocation) {
		try {
			OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			outpuStream = new FileOutputStream(new File(serverLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
