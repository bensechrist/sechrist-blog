package com.sechristfamily.blog.rest;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sechristfamily.blog.models.ImageModel;
import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.service.DeletePostService;
import com.sechristfamily.blog.service.PublishPostService;
import com.sechristfamily.blog.service.RetrievePostsService;

@Path("/posts")
public class PostEndpoint {
	
	@Inject
	private DeletePostService deleteService;
	
	@Inject
	private PublishPostService publishService;
	
	@Inject
	private RetrievePostsService service;
	
	@GET
	@Path("/{index}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PostSampleModel> allPosts(@PathParam("index") int index) {
		return service.getPosts(index);
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PostSampleModel> postsByTag(@QueryParam("tag") String tag) {
		return service.getPostsByTag(tag);
	}
	
	@DELETE
	@Path("/{id}")
	public Response deletePost(@PathParam("id") long id) {
		List<ImageModel> images = service.getEditPost(id).getImages();
		for (ImageModel image : images) {
			deleteImageFromDisk(image.getFilename());
		}
		deleteService.deletePost(id);
		return Response.ok().build();
	}
	
	void deleteImageFromDisk (String filename) {
		try {
			 
    		File file = new File(ImageRestEndpoint.SERVER_UPLOAD_LOCATION_FOLDER + filename);
 
    		if(file.delete()) {
    			System.out.println(file.getName() + " is deleted!");
    		} else {
    			System.out.println("Delete operation is failed for " + file.getName());
    		}
 
    	} catch (Exception e){
    		e.printStackTrace();
    	}
	}
	
	@POST
	@Path("/{id}")
	public Response publishPost(@PathParam("id") long id) {
		publishService.publishPost(id);
		return Response.ok().build();
	}
	
	@GET
	@Path("/search/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PostSampleModel> search(@PathParam("query") String query) {
		return service.search(query);
	}
	
}
