package com.sechristfamily.blog.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.service.TagService;

@Path("/tag")
public class TagRestEndpoint {
	
	@Inject
	protected TagService service;

	@GET
	@Path("/search/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Tag> searchTags(@PathParam("query") String query) {
		return service.search(query);
	}
}
