package com.sechristfamily.blog.models;

public interface TagModel {
	
	/**
	 * Get the url of the tag
	 * @return
	 */
	String getUrl();
	
	/**
	 * Get the name of the tag
	 * @return String
	 */
	String getName();
	
}
