package com.sechristfamily.blog.models;

public class ImageDTO implements ImageModel {

	private String filename;
	
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
}
