package com.sechristfamily.blog.models;

public interface ImageModel {
	
	/**
	 * Gets the filename of the image
	 * @return String
	 */
	String getFilename();
	
}
