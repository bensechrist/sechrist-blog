package com.sechristfamily.blog.models;

import java.util.Date;
import java.util.Set;

public interface PostViewModel {
	
	/**
	 * Gets the title of the post
	 * @return String
	 */
	String getTitle();
	
	/**
	 * Gets the text of the post
	 * @return String
	 */
	String getText();
	
	/**
	 * Gets the date posted
	 * @return Date
	 */
	Date getDatePosted();
	
	/**
	 * Returns whether the post is published
	 * @return Boolean
	 */
	Boolean isPublished();
	
	/**
	 * Gets the associated tags
	 * @return Set
	 */
	Set<TagModel> getTags();
	
	/**
	 * Gets the title of the next oldest post
	 * @return String
	 */
	String getOlderPostTitle();
	
	/**
	 * Gets the title of the next newest post
	 * @return String
	 */
	String getNewerPostTitle();

}
