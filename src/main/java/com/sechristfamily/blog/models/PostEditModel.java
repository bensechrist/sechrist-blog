package com.sechristfamily.blog.models;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface PostEditModel {
	
	/**
	 * Gets the id of the post
	 * @return
	 */
	Long getId();
	
	/**
	 * Gets the title of the post
	 * @return String
	 */
	String getTitle();
	
	/**
	 * Sets the title of the post
	 * @param title
	 */
	void setTitle(String title);
	
	/**
	 * Gets the text of the post
	 * @return String
	 */
	String getText();
	
	/**
	 * Sets the text of the post
	 * @param text
	 */
	void setText(String text);
	
	/**
	 * Gets the date posted
	 * @return Date
	 */
	Date getDatePosted();
	
	/**
	 * Sets the date the post was posted
	 * @param datePosted
	 */
	void setDatePosted(Date datePosted);
	
	/**
	 * Returns whether the post is published
	 * @return Boolean
	 */
	Boolean isPublished();
	
	/**
	 * Gets the associated tags
	 * @return Set
	 */
	Set<TagModel> getTags();
	
	/**
	 * Sets the associated tags
	 * @param tags
	 */
	void setTags(Set<TagModel> tags);
	
	/**
	 * Gets the associated images
	 * @return List
	 */
	List<ImageModel> getImages();
	
	/**
	 * Sets the associated images
	 * @param images
	 */
	void setImages(List<ImageModel> images);
	
	/**
	 * Gets the last edited date
	 * @return Date
	 */
	Date getLastEdited();
	
	/**
	 * Gets who edited the post last
	 * @return String
	 */
	String getWhoEdited();

}
