package com.sechristfamily.blog.models;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class PostEditDTO implements PostEditModel {
	
	private Long id;
	private String title;
	private String text;
	private Date datePosted;
	private Boolean isPublished;
	private Set<TagModel> tags;
	private List<ImageModel> images;
	private Date lastEdited;
	private String whoEdited;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}
	
	public Boolean isPublished() {
		return isPublished;
	}
	
	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}
	
	public Set<TagModel> getTags() {
		return tags;
	}
	
	public void setTags(Set<TagModel> tags) {
		this.tags = tags;
	}
	
	public List<ImageModel> getImages() {
		return images;
	}
	
	public void setImages(List<ImageModel> images) {
		this.images = images;
	}
	
	public Date getLastEdited() {
		return lastEdited;
	}
	
	public void setLastEdited(Date lastEdited) {
		this.lastEdited = lastEdited;
	}
	
	public String getWhoEdited() {
		return whoEdited;
	}
	
	public void setWhoEdited(String whoEdited) {
		this.whoEdited = whoEdited;
	}

}
