package com.sechristfamily.blog.models;

import java.util.Set;

public class PostSampleDTO implements PostSampleModel {

	private Long id;
	private String url;
	private String title;
	private String text;
	private String datePosted;
	private Boolean isPublished;
	private Set<TagModel> tags;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public Boolean isPublished() {
		return isPublished;
	}
	
	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}
	
	public Set<TagModel> getTags() {
		return tags;
	}
	
	public void setTags(Set<TagModel> tags) {
		this.tags = tags;
	}

}
