package com.sechristfamily.blog.models;

import java.util.Set;

public interface PostSampleModel {
	
	/**
	 * Gets the id of the post
	 * @return
	 */
	Long getId();
	
	/**
	 * Gets the url of the post
	 * @return
	 */
	String getUrl();
	
	/**
	 * Gets the title of the post
	 * @return title as a string
	 */
	String getTitle();
	
	/**
	 * Gets the text of the post
	 * @return text as a string
	 */
	String getText();
	
	/**
	 * Gets the date posted
	 * @return date as date object
	 */
	String getDatePosted();
	
	/**
	 * Returns whether the post is published
	 * @return boolean
	 */
	Boolean isPublished();
	
	/**
	 * Gets the associated tags
	 * @return List of Tags
	 */
	Set<TagModel> getTags();
	
}
