package com.sechristfamily.blog.models;

import java.util.Date;
import java.util.Set;

public class PostViewDTO implements PostViewModel {
	
	private String title;
	private String text;
	private Date datePosted;
	private Boolean isPublished;
	private Set<TagModel> tags;
	private String olderPostTitle;
	private String newerPostTitle;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Date getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}
	
	public Boolean isPublished() {
		return isPublished;
	}
	
	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}
	
	public Set<TagModel> getTags() {
		return tags;
	}
	
	public void setTags(Set<TagModel> tags) {
		this.tags = tags;
	}
	
	public String getOlderPostTitle() {
		return olderPostTitle;
	}
	
	public void setOlderPostTitle(String olderPostTitle) {
		this.olderPostTitle = olderPostTitle;
	}
	
	public String getNewerPostTitle() {
		return newerPostTitle;
	}
	
	public void setNewerPostTitle(String newerPostTitle) {
		this.newerPostTitle = newerPostTitle;
	}

}
