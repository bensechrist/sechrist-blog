package com.sechristfamily.blog.web;

import java.io.IOException;
import java.security.Principal;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakSecurityContext;
import org.keycloak.ServiceUrlConstants;
import org.keycloak.representations.IDToken;
import org.keycloak.util.KeycloakUriBuilder;

import com.sechristfamily.blog.security.MutableUserService;

@WebFilter("/*")
public class KeycloakAuthorizationFilter implements Filter {
	
	@Inject
	private MutableUserService userService;
	
	private String authUri = "https://dev.sechristfamily.com:9080/auth";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		IDToken idToken = getIDToken(httpRequest);
		if (idToken == null) {
			userService.setUserEmail(null);
			
			userService.setIsLoggedIn(false);
			
			String logoutUri = KeycloakUriBuilder.fromUri(authUri).path(ServiceUrlConstants.TOKEN_SERVICE_LOGOUT_PATH)
					.queryParam("redirect_uri", getContextUrl(httpRequest, "")).build("Sechrist Blog").toString();
			userService.setLogoutUri(logoutUri);
			
			String accountUri = KeycloakUriBuilder.fromUri(authUri).path(ServiceUrlConstants.ACCOUNT_SERVICE_PATH)
					.queryParam("referrer", "Blog").build("Sechrist Blog").toString();
			userService.setAccountUri(accountUri);
		} else {
			userService.setUserEmail(idToken.getEmail());
			
			Principal user = httpRequest.getUserPrincipal();
			userService.setIsLoggedIn(user != null && !user.getName().equals("anonymous"));
			
			String logoutUri = KeycloakUriBuilder.fromUri(authUri).path(ServiceUrlConstants.TOKEN_SERVICE_LOGOUT_PATH)
					.queryParam("redirect_uri", getContextUrl(httpRequest, "")).build("Sechrist Blog").toString();
			userService.setLogoutUri(logoutUri);
			
			String accountUri = KeycloakUriBuilder.fromUri(authUri).path(ServiceUrlConstants.ACCOUNT_SERVICE_PATH)
					.queryParam("referrer", "Blog").build("Sechrist Blog").toString();
			userService.setAccountUri(accountUri);
		}
		
		chain.doFilter(httpRequest, response);
	}
	
	private IDToken getIDToken(HttpServletRequest req) {
		KeycloakSecurityContext session = (KeycloakSecurityContext) req.getAttribute(KeycloakSecurityContext.class.getName());
		if (session == null)
			return null;
		return session.getIdToken();
	}
	
	private String getContextUrl(HttpServletRequest request, String path) {
		return request.getScheme() + "://" + request.getServerName() 
				+ ":" + request.getServerPort() + request.getContextPath() + path;
	}

	@Override
	public void destroy() {
		
	}

}
