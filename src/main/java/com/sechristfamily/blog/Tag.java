package com.sechristfamily.blog;

public interface Tag {
	
	/**
	 * Get the id of the tag
	 * @return Long
	 */
	Long getId();

	/**
	 * Get the name of the tag
	 * @return String
	 */
	String getName();
	
}
