package com.sechristfamily.blog.facelets;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.sechristfamily.blog.models.PostViewModel;

@Named
@RequestScoped
public class DisplayPostBean {

	private PostViewModel post;
	
	public PostViewModel getPost() {
		return post;
	}

	public void setPost(PostViewModel post) {
		this.post = post;
	}

}
