package com.sechristfamily.blog.facelets;

import java.io.IOException;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.sechristfamily.blog.security.MutableUserService;
import java.io.Serializable;

@Named
@SessionScoped
public class UserServiceBean implements Serializable, MutableUserService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1842007732299494581L;

	@Inject
	private FacesContext facesContext;

	private boolean isLoggedIn;
	private String logoutUri;
	private String accountUri;
	private String userEmail;
	
	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void setIsLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	
	public void login() {
		if (!isLoggedIn)
			facesContext.getExternalContext().invalidateSession();
		try {
			facesContext.getExternalContext().redirect(getContextUrl() + "/");
		} catch (IOException e) {
		}
	}
	
	public void logout() {
		facesContext.getExternalContext().invalidateSession();
		((HttpServletRequest)facesContext.getExternalContext()
		                .getRequest()).getSession().invalidate();
		try {
		        facesContext.getExternalContext()
		                .redirect(getLogoutUri());
		} catch (IOException e) {
		        // Do nothing
		}
	}
	
	public String getLogoutUri() {
		return logoutUri;
	}

	public void setLogoutUri(String logoutUri) {
		this.logoutUri = logoutUri;
	}
	
	public String getAccountUri() {
		return accountUri;
	}
	
	public void setAccountUri(String accountUri) {
		this.accountUri = accountUri;
	}
	
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	private String getContextUrl() {
		HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
		return request.getScheme() + "://" + request.getServerName() 
				+ ":" + request.getServerPort() + request.getContextPath();
	}

}
