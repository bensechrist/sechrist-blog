package com.sechristfamily.blog.facelets;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.service.UpdatePostException;
import com.sechristfamily.blog.service.UpdatePostService;

@Named
@SessionScoped
public class EditPostBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7603909497688454964L;

	protected PostEditModel updatedPost;
	
	@Inject
	protected UpdatePostService service;
	
	public String update() {
		try {
			service.updatePost(updatedPost);
			return AddPostBean.SUCCESS_ID;
		} catch (UpdatePostException e) {
			return null;
		}
	}
	
	public String save() {
		try {
			service.savePost(updatedPost);
			return AddPostBean.SUCCESS_ID;
		} catch (UpdatePostException e) {
			return null;
		}
	}
	
	public String publish() {
		try {
			service.publishPost(updatedPost);
			return AddPostBean.SUCCESS_ID;
		} catch (UpdatePostException e) {
			return null;
		}
	}
	
	public PostEditModel getPost () {
		return updatedPost;
	}
	
	public void setPost(PostEditModel post) {
		updatedPost = post;
	}

}
