package com.sechristfamily.blog.facelets;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.service.RetrievePostsService;

@Named
@ApplicationScoped
public class DisplayTagBean {

	private String tagName;
	
	@Inject
	protected RetrievePostsService service;
	
	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<PostSampleModel> getPosts() {
		return service.getPostsByTag(tagName);
	}
	
}
