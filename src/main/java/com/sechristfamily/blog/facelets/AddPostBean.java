package com.sechristfamily.blog.facelets;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.sechristfamily.blog.ImageFactory;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.service.AddPostException;
import com.sechristfamily.blog.service.AddPostService;

@Named
@SessionScoped
public class AddPostBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7319668554838147109L;

	static String SUCCESS_ID = "success";

	protected PostEditModel post;
	
	@Inject
	protected AddPostService postService;
	
	@Inject
	protected ImageFactory imageFactory;
	
	@PostConstruct
	public void init() {
		post = postService.newPost();
	}
	
	public PostEditModel getPost() {
		return post;
	}

	public String save() {
		try {
			postService.savePost(post);
			return SUCCESS_ID;
		} catch (AddPostException e) {
			return null;
		}
	}
	
	public String publish() {
		try {
			postService.publishPost(post);
			return SUCCESS_ID;
		} catch (AddPostException e) {
			return null;
		}
	}
}
