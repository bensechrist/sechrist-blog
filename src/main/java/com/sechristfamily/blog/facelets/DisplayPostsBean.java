package com.sechristfamily.blog.facelets;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.service.ImageService;
import com.sechristfamily.blog.service.RetrievePostsService;
import java.io.Serializable;

@Named
@SessionScoped
public class DisplayPostsBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7677427604951244581L;

	private String query;
	
	private int index = 1;
	
	private List<PostSampleModel> posts;
	
	private Date lastUpdated;
	
	private int numOfPages;
	
	private List<Image> randomImages;
	
	@Inject
	protected RetrievePostsService retrievePostsService;
	
	@Inject
	protected ImageService imageService;
	
	@PostConstruct
	public void init() {
		if (lastUpdated == null) {
			lastUpdated = new Date(0);
		}
		System.out.println("=================================== " + lastUpdated);
		Date fromDb = retrievePostsService.getLastUpdated();
		if ((fromDb == null) || (lastUpdated.getTime() < fromDb.getTime())
				|| (posts == null)) {
			lastUpdated = retrievePostsService.getLastUpdated();
			System.out.println("================================= Updating posts");
			posts = retrievePostsService.getPosts(index);
		}
		
		if (query != null)
			numOfPages = retrievePostsService.numberOfPages(query);
		else
			numOfPages = retrievePostsService.numberOfPages();
		
		randomImages = imageService.randomImages(4);
	}
	
	public List<Image> getRandomImages() {
		return randomImages;
	}
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public List<PostSampleModel> getPosts() {
		return posts;
	}
	
	public List<Integer> getIndexArray() {
		List<Integer> indexArray = new ArrayList<>();
		for (int i=0; i<getNumOfPages(); i++) {
			indexArray.add(i+1);
		}
		return indexArray;
	}

	public int getNumOfPages() {
		return numOfPages;
	}

}
