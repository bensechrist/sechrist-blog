package com.sechristfamily.blog;


public interface Image {
	
	/**
	 * Get the image id
	 * @return Long id
	 */
	Long getId();

	/**
	 * Get the name of the image file
	 * @return String name
	 */
	String getFileName();
	
	/**
	 * Set the name of the image file
	 * @param String name
	 */
	void setFileName(String fileName);
	
	/**
	 * Get the associated post
	 * @return Post post
	 */
	Post getPost();
	
	/**
	 * Set the associated post
	 * @param Post post
	 */
	void setPost(Post post);
}
