package com.sechristfamily.blog.security;

public interface UserService {
	
	boolean isLoggedIn();
	
//	void login();
//	
//	void logout();
	
	String getLogoutUri();
	
	String getAccountUri();
	
	String getUserEmail();

}
