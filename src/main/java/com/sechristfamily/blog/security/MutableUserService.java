package com.sechristfamily.blog.security;


public interface MutableUserService extends UserService {
	
	boolean isLoggedIn();
	
	void setIsLoggedIn(boolean isLoggedIn);
	
//	void login();
//	
//	void logout();
	
	String getLogoutUri();
	
	void setLogoutUri(String logoutUri);
	
	String getAccountUri();
	
	void setAccountUri(String accountUri);
	
	String getUserEmail();
	
	void setUserEmail(String userEmail);
	
}
