package com.sechristfamily.blog.domain;

import javax.enterprise.context.ApplicationScoped;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.ImageFactory;

@ApplicationScoped
public class ImageEntityFactory implements ImageFactory {

	@Override
	public Image newImage() {
		Image image = new ImageEntity();
		return image;
	}

}
