package com.sechristfamily.blog.domain;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sechristfamily.blog.Post;

@Entity
@Table(name = "post")
public class PostEntity implements Post {
	
	private Long id;
	
	private Long version;
	
	private String title;
	
	private String text;
	
	private Date datePosted;
	
	private Boolean isPublished;
	
	private Set<TagEntity> tags;
	
	private List<ImageEntity> images;
	
	private Date lastEdited;
	
	private String whoEdited;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Version
	public Long getVersion() {
		return version;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(nullable = false, unique = true)
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	@Lob
	@Column(nullable = false, columnDefinition = "TEXT")
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	@Column(nullable = false)
	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	public Set<TagEntity> getTags() {
		return tags;
	}

	public void setTags(Set<TagEntity> tags) {
		this.tags = tags;
	}

	@Column
	public Boolean isPublished() {
		return isPublished;
	}

	public void setPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "post", cascade = { CascadeType.ALL })
	public List<ImageEntity> getImages() {
		return images;
	}

	public void setImages(List<ImageEntity> images) {
		this.images = images;
	}

	@Column
	public Date getLastEdited() {
		return lastEdited;
	}

	public void setLastEdited(Date date) {
		this.lastEdited = date;
	}

	@Column
	public String getWhoEdited() {
		return whoEdited;
	}

	public void setWhoEdited(String whoEdited) {
		this.whoEdited = whoEdited;
	}

}
