package com.sechristfamily.blog;

public interface ImageFactory {

	Image newImage();
}
