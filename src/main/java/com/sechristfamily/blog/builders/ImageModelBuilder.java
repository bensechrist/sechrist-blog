package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.models.ImageModel;

public interface ImageModelBuilder {
	
	/**
	 * Set the image to be used for the model
	 * @param image
	 * @return
	 */
	ImageModelBuilder setImage(Image image);
	
	/**
	 * Build the image model
	 * @return
	 */
	ImageModel build();

}
