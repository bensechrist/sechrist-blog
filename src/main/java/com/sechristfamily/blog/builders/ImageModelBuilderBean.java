package com.sechristfamily.blog.builders;

import javax.enterprise.context.RequestScoped;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.models.ImageDTO;
import com.sechristfamily.blog.models.ImageModel;

@RequestScoped
public class ImageModelBuilderBean implements ImageModelBuilder {
	
	private ImageDTO image;

	@Override
	public ImageModelBuilder setImage(Image image) {
		this.image = new ImageDTO();
		this.image.setFilename(image.getFileName());
		return this;
	}

	@Override
	public ImageModel build() {
		return image;
	}

}
