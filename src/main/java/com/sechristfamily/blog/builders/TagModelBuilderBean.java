package com.sechristfamily.blog.builders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.enterprise.context.RequestScoped;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.TagDTO;
import com.sechristfamily.blog.models.TagModel;

@RequestScoped
public class TagModelBuilderBean implements TagModelBuilder {
	
	private TagDTO tag;
	
	@Override
	public TagModelBuilder setTag(Tag tag) {
		this.tag = new TagDTO();
		String escapedName = "";
		try {
			escapedName = URLEncoder.encode(tag.getName(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Do nothing
		}
		this.tag.setUrl("tags/" +  escapedName);
		this.tag.setName(tag.getName());
		return this;
	}

	@Override
	public TagModel build() {
		return tag;
	}

}
