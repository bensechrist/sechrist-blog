package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostViewModel;

public interface PostViewModelBuilder {
	
	/**
	 * Set the post
	 * @param post
	 * @return
	 */
	PostViewModelBuilder setPost(Post post);
	
	/**
	 * Build the model from the set post
	 * @return
	 */
	PostViewModel build();

}
