package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostSampleModel;

public interface PostSampleModelBuilder {
	
	/**
	 * Sets the appropriate attributes for the post sample
	 * @param post
	 * @return
	 */
	PostSampleModelBuilder setPost(Post post);
	
	/**
	 * Builds the post sample
	 * @return
	 */
	PostSampleModel build();

}
