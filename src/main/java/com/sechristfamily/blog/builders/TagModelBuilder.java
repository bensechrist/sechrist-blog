package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.TagModel;

public interface TagModelBuilder {
	
	TagModelBuilder setTag(Tag tag);
	
	TagModel build();

}
