package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostEditModel;

public interface PostEditModelBuilder {
	
	/**
	 * Set the post
	 * @param Post
	 * @return PostEditModelBuilder
	 */
	PostEditModelBuilder setPost(Post post);
	
	/**
	 * Builds the model
	 * @return PostEditModel
	 */
	PostEditModel build();

}
