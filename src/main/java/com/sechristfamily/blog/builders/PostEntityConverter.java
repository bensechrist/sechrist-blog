package com.sechristfamily.blog.builders;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostEditModel;

public interface PostEntityConverter {
	
	Post convert(PostEditModel model);

}
