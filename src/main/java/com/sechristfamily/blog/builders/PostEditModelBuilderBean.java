package com.sechristfamily.blog.builders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.RequestScoped;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.ImageDTO;
import com.sechristfamily.blog.models.ImageModel;
import com.sechristfamily.blog.models.PostEditDTO;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.models.TagDTO;
import com.sechristfamily.blog.models.TagModel;

@RequestScoped
public class PostEditModelBuilderBean implements PostEditModelBuilder {
	
	private PostEditDTO postEdit;

	@Override
	public PostEditModelBuilder setPost(Post post) {
		postEdit = new PostEditDTO();
		postEdit.setId(post.getId());
		postEdit.setTitle(post.getTitle());
		postEdit.setText(post.getText());
		postEdit.setDatePosted(post.getDatePosted());
		postEdit.setIsPublished(post.isPublished());
		Set<TagModel> tags = new HashSet<>();
		for (Tag t : post.getTags()) {
			TagDTO tag = new TagDTO();
			tag.setName(t.getName());
			tags.add(tag);
		}
		postEdit.setTags(tags);
		List<ImageModel> images = new ArrayList<>();
		for (Image i : post.getImages()) {
			ImageDTO image = new ImageDTO();
			image.setFilename(i.getFileName());
			images.add(image);
		}
		postEdit.setImages(images);
		postEdit.setLastEdited(post.getLastEdited());
		postEdit.setWhoEdited(post.getWhoEdited());
		return this;
	}

	@Override
	public PostEditModel build() {
		return postEdit;
	}

}
