package com.sechristfamily.blog.builders;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.jsoup.Jsoup;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.PostSampleDTO;
import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.models.TagModel;

@RequestScoped
public class PostSampleModelBuilderBean implements PostSampleModelBuilder {
	
	private PostSampleDTO postSample;
	
	@Inject
	TagModelBuilder tagModelBuilder;

	@Override
	public PostSampleModelBuilder setPost(Post post) {
		postSample = new PostSampleDTO();
		postSample.setId(post.getId());
		postSample.setUrl(post.getTitle().replaceAll("[^A-Za-z0-9/!]+", "."));
		postSample.setTitle(post.getTitle());
		postSample.setText(shortenText(post.getText()));
		postSample.setDatePosted(new SimpleDateFormat("dd.MMMM.yyyy").format(post.getDatePosted()));
		postSample.setIsPublished(post.isPublished());
		Set<TagModel> tags = new HashSet<>();
		for (Tag t : post.getTags()) {
			tags.add(tagModelBuilder.setTag(t).build());
		}
		postSample.setTags(tags);
		return this;
	}
	
	private String shortenText(String text) {
		text = Jsoup.parse(text).text();
		if (text.length() > 250)
			return text.substring(0, 250).concat("...");
		else
			return text;
	}

	@Override
	public PostSampleModel build() {
		return postSample;
	}

}
