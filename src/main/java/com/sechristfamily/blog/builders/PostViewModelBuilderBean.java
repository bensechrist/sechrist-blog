package com.sechristfamily.blog.builders;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.PostViewDTO;
import com.sechristfamily.blog.models.PostViewModel;
import com.sechristfamily.blog.models.TagDTO;
import com.sechristfamily.blog.models.TagModel;
import com.sechristfamily.blog.service.RetrievePostsService;

@RequestScoped
public class PostViewModelBuilderBean implements PostViewModelBuilder {

	private PostViewDTO postView;
	
	@Inject
	private RetrievePostsService service;
	
	@Override
	public PostViewModelBuilder setPost(Post post) {
		postView = new PostViewDTO();
		postView.setTitle(post.getTitle());
		postView.setText(post.getText());
		postView.setDatePosted(post.getDatePosted());
		postView.setIsPublished(post.isPublished());
		Set<TagModel> tags = new HashSet<>();
		for (Tag t : post.getTags()) {
			TagDTO tag = new TagDTO();
			tag.setName(t.getName());
			tags.add(tag);
		}
		postView.setTags(tags);
		postView.setOlderPostTitle(service.getOlderPostTitle(post));
		postView.setNewerPostTitle(service.getNewerPostTitle(post));
		return this;
	}

	@Override
	public PostViewModel build() {
		return postView;
	}

}
