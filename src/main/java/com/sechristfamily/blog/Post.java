package com.sechristfamily.blog;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface Post {
	
	/**
	 * Gets the id of the post
	 * @return Long id
	 */
	Long getId();
	
	/**
	 * Gets the title of the post
	 * @return title as a string
	 */
	String getTitle();
	
	/**
	 * Gets the text of the post
	 * @return text as a string
	 */
	String getText();
	
	/**
	 * Gets the date posted
	 * @return date as date object
	 */
	Date getDatePosted();
	
	/**
	 * Gets the associated tags
	 * @return List of Tags
	 */
	Set<? extends Tag> getTags();
	
	/**
	 * Gets the images for the post
	 * @return List of Images
	 */
	List<? extends Image> getImages();
	
	/**
	 * Returns whether the post is published
	 * @return boolean
	 */
	Boolean isPublished();
	
	/**
	 * Gets the last time the post was updated
	 * @return Date
	 */
	Date getLastEdited();
	
	/**
	 * Gets the email of who edited the post last
	 * @return String
	 */
	String getWhoEdited();
	
}
