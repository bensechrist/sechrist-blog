package com.sechristfamily.blog.service;

import java.util.List;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.models.TagModel;

public interface TagService {

	TagModel getTag(String name);
	
	List<Tag> search(String query);
	
	void deleteTag(String name);
}
