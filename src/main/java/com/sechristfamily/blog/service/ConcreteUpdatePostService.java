package com.sechristfamily.blog.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.builders.PostEntityConverter;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.repository.PostRepository;

@ApplicationScoped
@Transactional
public class ConcreteUpdatePostService implements UpdatePostService {
	
	@Inject
	protected PostRepository repository;
	
	@Inject
	protected PostEntityConverter converter;

	@Override
	public void updatePost(PostEditModel post) throws UpdatePostException {
		repository.update(converter.convert(post));
	}

	@Override
	public void savePost(PostEditModel post) throws UpdatePostException {
		repository.updateSave(converter.convert(post));
	}

	@Override
	public void publishPost(PostEditModel post) throws UpdatePostException {
		repository.publish(converter.convert(post));
	}

}
