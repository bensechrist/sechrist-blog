package com.sechristfamily.blog.service;

import com.sechristfamily.blog.models.PostEditModel;

public interface UpdatePostService {

	void updatePost(PostEditModel updatedPost) throws UpdatePostException;
	
	void savePost(PostEditModel updatedPost) throws UpdatePostException;
	
	void publishPost(PostEditModel updatedPost) throws UpdatePostException;
}
