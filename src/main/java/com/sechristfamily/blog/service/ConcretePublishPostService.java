package com.sechristfamily.blog.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.repository.PostRepository;

@ApplicationScoped
@Transactional
public class ConcretePublishPostService implements PublishPostService {

	@Inject
	protected PostRepository repository;
	
	@Override
	public void publishPost(long id) {
		Post post = repository.get(id);
		repository.publish(post);
	}

}
