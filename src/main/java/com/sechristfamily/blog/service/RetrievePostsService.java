package com.sechristfamily.blog.service;

import java.util.Date;
import java.util.List;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.models.PostViewModel;

public interface RetrievePostsService {

	List<PostSampleModel> getPosts(int index);
	
	PostEditModel getEditPost(Long id);
	
	PostViewModel getViewPost(String arg2);
	
	List<PostSampleModel> getPostsByTag(String name);
	
	List<PostSampleModel> search(String query);

	int numberOfPages();
	
	int numberOfPages(String query);

	String getNewerPostTitle(Post post);

	String getOlderPostTitle(Post post);

	Date getLastUpdated();

}
