package com.sechristfamily.blog.service;

import com.sechristfamily.blog.models.PostEditModel;

public interface AddPostService {

	PostEditModel newPost();
	
	void savePost(PostEditModel post) throws AddPostException;
	
	void publishPost(PostEditModel post) throws AddPostException;
}
