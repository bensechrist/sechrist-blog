package com.sechristfamily.blog.service;

import java.util.List;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.models.ImageModel;

public interface ImageService {
	
	ImageModel getImage(String filename);
	
	List<Image> randomImages(int numImgs);
	
	boolean imageExistsForPost(Image image);
}
