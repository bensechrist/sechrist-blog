package com.sechristfamily.blog.service;

public interface DeletePostService {

	void deletePost(long id);
}
