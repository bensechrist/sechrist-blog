package com.sechristfamily.blog.service;

import java.util.Date;

public interface DateTimeService {

	Date getCurrentDate();
}
