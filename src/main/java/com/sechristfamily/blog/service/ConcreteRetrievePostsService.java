package com.sechristfamily.blog.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.builders.PostEditModelBuilder;
import com.sechristfamily.blog.builders.PostSampleModelBuilder;
import com.sechristfamily.blog.builders.PostViewModelBuilder;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.models.PostSampleModel;
import com.sechristfamily.blog.models.PostViewModel;
import com.sechristfamily.blog.repository.PostRepository;
import com.sechristfamily.blog.security.UserService;

@ApplicationScoped
public class ConcreteRetrievePostsService implements RetrievePostsService {

	@Inject
	protected PostRepository repository;
	
	@Inject
	protected UserService userBean;
	
	@Inject
	protected PostSampleModelBuilder postSampleBuilder;
	
	@Inject
	protected PostEditModelBuilder postEditBuilder;
	
	@Inject
	protected PostViewModelBuilder postViewBuilder;
	
	@Override
	public List<PostSampleModel> getPosts(int index) {
		List<Post> posts = new ArrayList<>();
		if (userBean.isLoggedIn())
			posts = repository.getAll(index);
		else
			posts = repository.getPublished(index);
		List<PostSampleModel> models = new ArrayList<>();
		for (Post p : posts) {
			models.add(postSampleBuilder.setPost(p).build());
		}
		return models;
	}
	
	@Transactional
	@Override
	public PostEditModel getEditPost(Long id) {
		Post post = repository.get(id);
		post.getImages();
		return postEditBuilder.setPost(post).build();
	}
	
	@Override
	public PostViewModel getViewPost(String title) {
		Post post = repository.get(title);
		if (!post.isPublished() && !userBean.isLoggedIn())
			return null;
		return postViewBuilder.setPost(post).build();
	}

	@Override
	public List<PostSampleModel> getPostsByTag(String name) {
		List<Post> posts = new ArrayList<>();
		if (userBean.isLoggedIn())
			posts = repository.getAllByTag(name);
		else
			posts = repository.getPublishedByTag(name);
		List<PostSampleModel> models = new ArrayList<>();
		for (Post p : posts) {
			models.add(postSampleBuilder.setPost(p).build());
		}
		return models;
	}

	@Override
	public List<PostSampleModel> search(String query) {
		List<Post> posts = new ArrayList<>();
		if (userBean.isLoggedIn())
			posts = repository.searchAll(query);
		else
			posts = repository.search(query);
		List<PostSampleModel> models = new ArrayList<>();
		for (Post p : posts) {
			models.add(postSampleBuilder.setPost(p).build());
		}
		return models;
	}

	@Override
	public int numberOfPages() {
		return repository.numberOfPages();
	}
	
	@Override
	public int numberOfPages(String query) {
		return repository.numberOfPages(query);
	}

	@Override
	public String getNewerPostTitle(Post post) {
		if (userBean.isLoggedIn())
			return repository.getNewerPostTitle(post.getDatePosted());
		else
			return repository.getNewerPublishedPostTitle(post.getDatePosted());
	}

	@Override
	public String getOlderPostTitle(Post post) {
		if (userBean.isLoggedIn())
			return repository.getOlderPostTitle(post.getDatePosted());
		else
			return repository.getOlderPublishedPostTitle(post.getDatePosted());
	}
	
	@Override
	public Date getLastUpdated() {
		return repository.getLastUpdatedDate();
	}

}
