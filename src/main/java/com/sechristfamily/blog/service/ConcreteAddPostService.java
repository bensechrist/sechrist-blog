package com.sechristfamily.blog.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import com.sechristfamily.blog.builders.PostEntityConverter;
import com.sechristfamily.blog.models.PostEditDTO;
import com.sechristfamily.blog.models.PostEditModel;
import com.sechristfamily.blog.repository.PostRepository;

@ApplicationScoped
@Transactional
public class ConcreteAddPostService implements AddPostService {
	
	@Inject
	protected PostRepository repository;
	
	@Inject
	protected PostEntityConverter converter;
	
	@Inject
	protected DateTimeService dateService;
	
	@Override
	public PostEditModel newPost() {
		PostEditDTO post = new PostEditDTO();
		post.setDatePosted(dateService.getCurrentDate());
		return post;
	}
	
	@Override
	public void savePost(PostEditModel post) throws AddPostException {
		try {
			repository.saveForLater(converter.convert(post));
		} catch (PersistenceException e) {
			throw new AddPostException();
		}
	}

	@Override
	public void publishPost(PostEditModel post) throws AddPostException {
		try {
			repository.add(converter.convert(post));
		} catch (PersistenceException e) {
	    	throw new AddPostException();
	    }
	}

}
