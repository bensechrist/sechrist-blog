package com.sechristfamily.blog.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.Image;
import com.sechristfamily.blog.builders.ImageModelBuilder;
import com.sechristfamily.blog.models.ImageModel;
import com.sechristfamily.blog.repository.ImageRepository;

@ApplicationScoped
@Transactional
public class ConcreteImageService implements ImageService {
	
	@Inject
	private ImageRepository repository;
	
	@Inject
	private ImageModelBuilder builder;
	
	@Override
	public ImageModel getImage(String filename) {
		Image image;
		if (repository.exists(filename))
			image = repository.get(filename);
		else
			image = repository.newImage(filename);
		return builder.setImage(image).build();
	}

	@Override
	public List<Image> randomImages(int numImgs) {
		return repository.random(numImgs);
	}

	@Override
	public boolean imageExistsForPost(Image image) {
		return repository.exists(image.getFileName());
	}

}
