package com.sechristfamily.blog.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.repository.PostRepository;

@ApplicationScoped
@Transactional
public class ConcreteDeletePostService implements DeletePostService {

	@Inject
	private PostRepository repository;
	
	@Override
	public void deletePost(long id) {
		repository.delete(id);
	}

}
