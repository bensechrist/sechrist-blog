package com.sechristfamily.blog.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.builders.TagModelBuilder;
import com.sechristfamily.blog.models.TagModel;
import com.sechristfamily.blog.repository.TagRepository;

@ApplicationScoped
@Transactional
public class ConcreteTagService implements TagService {

	@Inject
	protected TagRepository repository;
	
	@Inject
	protected TagModelBuilder builder;
	
	@Override
	public TagModel getTag(String name) {
		if (repository.tagExists(name))
			return builder.setTag(repository.getTag(name)).build();
		else
			return builder.setTag(repository.newTag(name)).build();
	}

	@Override
	public List<Tag> search(String query) {
		return repository.search(query);
	}

	@Override
	public void deleteTag(String name) {
		repository.delete(name);
	}

}
