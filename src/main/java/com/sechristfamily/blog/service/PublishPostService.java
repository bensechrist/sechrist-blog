package com.sechristfamily.blog.service;

public interface PublishPostService {
	
	void publishPost(long id);
}
