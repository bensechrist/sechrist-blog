package com.sechristfamily.blog.tests;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.archive.importer.MavenImporter;

import com.sechristfamily.blog.service.AlwaysLoggedInService;
import com.sechristfamily.blog.service.AlwaysLoggedOutService;

public class Deployments {
	
	public static final String ALWAYS_LOGGED_IN_BEAN = "beans-loggedInAlt.xml";
	public static final String ALWAYS_LOGGED_OUT_BEAN = "beans-loggedOutAlt.xml";

	public static WebArchive createFullDeployment() {
		return createFullDeployment(null);
	}
	
	public static WebArchive createFullDeployment(String beansFile) {
		WebArchive archive = ShrinkWrap.create(MavenImporter.class)
			        .loadPomFromFile("pom.xml")
			        .importBuildOutput()
			        .as(WebArchive.class)
			        .addAsWebInfResource("persistence-test.xml", "classes/META-INF/persistence.xml")
			        .addClass(AlwaysLoggedInService.class)
			        .addClass(AlwaysLoggedOutService.class)
			        .addAsWebInfResource("web.xml");
		if (beansFile != null) {
			archive.addAsWebInfResource(beansFile, "beans.xml");
		}
		return archive;
	}
}
