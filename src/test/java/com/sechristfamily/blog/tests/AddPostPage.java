package com.sechristfamily.blog.tests;

import static org.jboss.arquillian.graphene.Graphene.*;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.findby.FindByJQuery;
import org.jboss.arquillian.graphene.page.Location;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Location("posts/admin/add.xhtml")
public class AddPostPage {

	@FindBy
	private WebElement title;
	
	@FindBy
	private WebElement postText;
	
	@FindBy
	private WebElement postTags;
	
	@FindByJQuery("input[type=file]")
	private WebElement imageNames;
	
	@FindBy
	private WebElement publishBtn;
	
	@FindBy
	private WebElement saveBtn;
	
	@FindByJQuery("a.btn.pull-right")
	private WebElement cancelBtn;
	
	@Drone
	private WebDriver browser;
	
	public void assertOnPage() {
		assertTrue(browser.getTitle().contains("Add Post"));
	}
	
	public void saveNewPost(String title, String postText, 
			List<String> imageNames) {
		this.title.sendKeys(title);
		this.postText.sendKeys(postText);
//		for (String tag : tags) {
//			this.postTags.sendKeys(tag);
//			this.postTags.sendKeys(Keys.RETURN);
//		}
//		for (String imageName : imageNames) {
//			if (!imageName.isEmpty())
//				this.imageNames.sendKeys(System.getProperty("user.home") + "/seleniumFileUpload/" + imageName);
//		}
		guardHttp(saveBtn).click();
	}
	
	public void assertErrorPresent(String error) {
		assertTrue(browser.findElement(By.className("text-danger")).getText().contains(error));
	}
	
	public void publishNewPost(String title, String postText, 
			List<String> imageNames) {
		this.title.sendKeys(title);
		this.postText.sendKeys(postText);
//		for (String tag : tags) {
//			this.postTags.sendKeys(tag);
//			this.postTags.sendKeys(Keys.RETURN);
//		}
//		for (String imageName : imageNames) {
//			if (!imageName.isEmpty())
//				this.imageNames.sendKeys(System.getProperty("user.home") + "/seleniumFileUpload/" + imageName);
//		}
		publishBtn.click();
		try {
			Alert alert = browser.switchTo().alert();
			String AlertText = alert.getText();
			System.out.println("===================== " + AlertText);
	        alert.accept();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}
}
