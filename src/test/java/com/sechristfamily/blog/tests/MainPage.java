package com.sechristfamily.blog.tests;

import static org.jboss.arquillian.graphene.Graphene.guardHttp;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Scanner;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.findby.FindByJQuery;
import org.jboss.arquillian.graphene.page.Location;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.sechristfamily.blog.Post;

@Location("index.xhtml")
public class MainPage {
	
	@FindByJQuery(".nav > li > a")
	private WebElement loginBtn;
	
	@FindByJQuery(".dropdown-menu > li > a")
	private WebElement newPostBtn;
	
	@FindBy(className = "postTitle")
	private WebElement postTitle;
	
	@FindBy(className = "deletePost")
	private WebElement deletePostBtn;
	
	@Drone
	private WebDriver browser;
	
	public void assertOnPage() {
		try {
			assertEquals("Sechrist Blog", browser.getTitle().trim());
		} catch (Throwable e) {
			e.printStackTrace(System.err);
			throw e;
		}
	}
	
	public void login() {
		if (LoginCreds.username == null) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Username: ");
			LoginCreds.username = scanner.nextLine();
			System.out.print("Password: ");
			LoginCreds.passwd = scanner.nextLine();
			scanner.close();
		}
		guardHttp(loginBtn).click();
		WebElement username = browser.findElement(By.cssSelector("#username"));
		WebElement password = browser.findElement(By.cssSelector("#password"));
		WebElement login = browser.findElement(By.cssSelector("#kc-login"));
		username.sendKeys(LoginCreds.username);
		password.sendKeys(LoginCreds.passwd);
		guardHttp(login).click();
	}
	
	public void assertPostPresent(Post post) {
		assertTrue(postTitle.getText().contains(post.getTitle()));
		// Do more later :)
	}

	public void removePost(Post post) {
		deletePostBtn.click();
		try {
			Alert alert = browser.switchTo().alert();
			String AlertText = alert.getText();
	        System.out.println("===================== " + AlertText);
	        alert.accept();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

}
