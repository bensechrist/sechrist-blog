package com.sechristfamily.blog.tests;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.graphene.page.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.domain.PostEntity;

@RunWith(Arquillian.class)
public class AddPostFT {
	
	@Deployment(testable = false)
	public static WebArchive createDeployment() {
		return Deployments.createFullDeployment(Deployments.ALWAYS_LOGGED_IN_BEAN);
	}
	
	@Drone
    private WebDriver browser;
	
	@ArquillianResource
	private URL deploymentUrl;
	
	@Page
	private MainPage mainPage;
	
	private String testTitle = "Test";
	private String testText = "This is a test post.";
	private String titleError = "Post Title Required";
	private String textError = "Post Text Required";
	
	@Test
	public void TestSavePostSuccess(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle(testTitle);
		post.setText(testText);
		List<String> imageNames = new ArrayList<String>();
		imageNames.add("image.jpg");
		addPostPage.saveNewPost(post.getTitle(), post.getText(), imageNames);
		mainPage.assertOnPage();
		mainPage.assertPostPresent(post);
		mainPage.removePost(post);
	}
	
	@Test
	public void TestSavePostNoTitle(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle("");
		post.setText(testText);
		addPostPage.saveNewPost(post.getTitle(), post.getText(), new ArrayList<String>());
		addPostPage.assertOnPage();
		addPostPage.assertErrorPresent(titleError);
	}
	
	@Test
	public void TestSavePostNoText(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle(testTitle);
		post.setText("");
		addPostPage.saveNewPost(post.getTitle(), post.getText(), new ArrayList<String>());
		addPostPage.assertOnPage();
		addPostPage.assertErrorPresent(textError);
	}
	
	@Test
	public void TestPublishPostSuccess(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle(testTitle);
		post.setText(testText);
		List<String> imageNames = new ArrayList<String>();
		imageNames.add("image.jpg");
		addPostPage.publishNewPost(post.getTitle(), post.getText(), imageNames);
		mainPage.assertOnPage();
		mainPage.assertPostPresent(post);
		mainPage.removePost(post);
	}
	
	@Test
	public void TestPublishPostNoTitle(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle("");
		post.setText(testText);
		addPostPage.publishNewPost(post.getTitle(), post.getText(), new ArrayList<String>());
		addPostPage.assertOnPage();
		addPostPage.assertErrorPresent(titleError);
	}
	
	@Test
	public void TestPublishPostNoText(@InitialPage AddPostPage addPostPage) {
		addPostPage.assertOnPage();
		Post post = new PostEntity();
		post.setTitle(testTitle);
		post.setText("");
		addPostPage.publishNewPost(post.getTitle(), post.getText(), new ArrayList<String>());
		addPostPage.assertOnPage();
		addPostPage.assertErrorPresent(textError);
	}
}
