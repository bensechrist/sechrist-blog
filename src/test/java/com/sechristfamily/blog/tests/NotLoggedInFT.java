package com.sechristfamily.blog.tests;

import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@RunWith(Arquillian.class)
public class NotLoggedInFT {

	@Deployment(testable = false)
	public static WebArchive createDeployment() {
		WebArchive archive = Deployments.createFullDeployment(Deployments.ALWAYS_LOGGED_OUT_BEAN);
		archive.addAsWebInfResource("persistence-test-populate-db.xml", "classes/META-INF/persistence.xml");
		archive.addAsWebInfResource("sql/published_posts.sql", "classes/META-INF/data.sql");
		return archive;
	}
	
	@Drone
	private WebDriver browser;
	
	@ArquillianResource
	private URL deploymentUrl;
	
	@FindBy(css = "body")
	private WebElement body;
	
	@Test
	public void TestAddPageNotLoggedIn(@InitialPage AddPostPage addPostPage) {
		assertTrue(body.getText().contains("403 - Forbidden"));
	}
	
	@Test
	public void TestEditPageNotLoggedIn() {
		browser.get(deploymentUrl.toExternalForm() + "posts/admin/edit.xhtml?id=0");
		assertTrue(body.getText().contains("403 - Forbidden"));
	}
	
}
