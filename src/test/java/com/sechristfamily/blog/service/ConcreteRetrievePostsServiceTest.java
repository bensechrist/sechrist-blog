package com.sechristfamily.blog.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.domain.PostEntity;
import com.sechristfamily.blog.facelets.UserBean;
import com.sechristfamily.blog.repository.PostRepository;

public class ConcreteRetrievePostsServiceTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
      }};
	
	@Mock
	private PostRepository repository;
	
	@Mock
	private UserBean userBean;
	
	private ConcreteRetrievePostsService service = new ConcreteRetrievePostsService();
	
	@Before
	public void setup() {
		service.repository = repository;
		service.userBean = userBean;
	}
	
	@Test
	public void testRetrieveAllSuccess() {
		final List<Post> posts = new ArrayList<Post>();
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(true));
			oneOf(repository).getAll();
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.getPosts();
		assertEquals(posts, actualPosts);
	}
	
	@Test
	public void testRetrievePublishedSuccess() {
		final List<Post> posts = new ArrayList<Post>();
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(false));
			oneOf(repository).getPublished();
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.getPosts();
		assertEquals(posts, actualPosts);
	}
	
	@Test
	public void testGetPostById() {
		final Post post = new PostEntity();
		final Long id = (long) 0;
		context.checking(new Expectations() { { 
			oneOf(repository).get(id);
			will(returnValue(post));
		} });
		
		Post actualPost = service.getPost(id);
		assertEquals(post, actualPost);
	}
	
	@Test
	public void testGetAllPostsByTag() {
		final List<Post> posts = new ArrayList<Post>();
		final String name = "Test";
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(true));
			oneOf(repository).getAllByTag(name);
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.getPostsByTag(name);
		assertEquals(posts, actualPosts);
	}
	
	@Test
	public void testGetPublishedPostsByTag() {
		final List<Post> posts = new ArrayList<Post>();
		final String name = "Test";
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(false));
			oneOf(repository).getPublishedByTag(name);
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.getPostsByTag(name);
		assertEquals(posts, actualPosts);
	}
	
	@Test
	public void testSearchAll() {
		final List<Post> posts = new ArrayList<Post>();
		final String query = "Test";
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(true));
			oneOf(repository).searchAll(query);
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.search(query);
		assertEquals(posts, actualPosts);
	}
	
	@Test
	public void testSearchPublished() {
		final List<Post> posts = new ArrayList<Post>();
		final String query = "Test";
		context.checking(new Expectations() { { 
			oneOf(userBean).isLoggedIn();
			will(returnValue(false));
			oneOf(repository).search(query);
			will(returnValue(posts));
		} });
		
		List<Post> actualPosts = service.search(query);
		assertEquals(posts, actualPosts);
	}
}
