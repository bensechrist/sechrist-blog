package com.sechristfamily.blog.service;

import static org.jmock.Expectations.returnValue;
import static org.jmock.Expectations.throwException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.PersistenceException;

import org.jmock.Expectations;
import org.jmock.api.Action;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.PostFactory;
import com.sechristfamily.blog.repository.PostRepository;

public class ConcreteAddPostServiceTest {
	
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();
	
	@Mock
	private Post post;
	
	@Mock
	private PostFactory postFactory;
	
	@Mock
	private PostRepository repository;
	
	@Mock
	private DateTimeService dateService;

	private ConcreteAddPostService service = new ConcreteAddPostService();
	
	@Before
	public void setup() {
		service.repository = repository;
		service.postFactory = postFactory;
		service.dateService = dateService;
	}
	
	@Test
	public void testNewPost() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateWithoutTime = null;
	    try {
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			// Do nothing
		}
		final Date date = dateWithoutTime;
		context.checking(new Expectations() { {
			oneOf(postFactory).newPost();
			will(returnValue(post));
			oneOf(dateService).getCurrentDate();
			will(returnValue(date));
			oneOf(post).setDatePosted(date);
			will(returnValue(null));
		} });
		
		service.newPost();
	}
	
	@Test
	public void testSavePostSuccess() throws Exception {
		context.checking(savePostExpectations(returnValue(null)));
		
		service.savePost(post);
	}

	@Test(expected=AddPostException.class)
	public void testSavePostFailure() throws Exception {
		context.checking(savePostExpectations(throwException(new PersistenceException())));
		
		service.savePost(post);
	}
	
	@Test
	public void testPublishPostSuccess() throws Exception {
		context.checking(newPostExpectations(returnValue(null)));
		
		service.publishPost(post);
	}
	
	@Test(expected=AddPostException.class)
	public void testPublishPostFailure() throws Exception {
		context.checking(newPostExpectations(throwException(new PersistenceException())));
		
		service.publishPost(post);
	}

	private Expectations newPostExpectations(final Action outcome) {
		return new Expectations() { { 
			oneOf(repository).add(with(same(post)));
			will(outcome);
		} };
	}
	
	private Expectations savePostExpectations(final Action outcome) {
		return new Expectations() { { 
			oneOf(repository).saveForLater(with(same(post)));
			will(outcome);
		} };
	}
}
