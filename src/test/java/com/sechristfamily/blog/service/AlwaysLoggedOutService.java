package com.sechristfamily.blog.service;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

@Alternative
@ApplicationScoped
public class AlwaysLoggedOutService implements UserService {

	@Override
	public boolean isLoggedIn() {
		return false;
	}

	@Override
	public void login() {
		
	}

	@Override
	public void logout() {
		
	}

	@Override
	public String logoutUri() {
		return "";
	}

	@Override
	public String accountUri() {
		return "";
	}

	@Override
	public String getUserEmail() {
		return "";
	}

}
