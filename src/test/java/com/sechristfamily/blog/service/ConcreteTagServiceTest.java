package com.sechristfamily.blog.service;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Tag;
import com.sechristfamily.blog.repository.TagRepository;

public class ConcreteTagServiceTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();
	
	@Mock
	private TagRepository repository;
	
	private ConcreteTagService service = new ConcreteTagService();
	
	private String name = "Test";
	
	@Before
	public void init() {
		service.repository = repository;
	}
	
	@Test
	public void testGetTagExists() {
		context.checking(new Expectations() { { 
			oneOf(repository).tagExists(with(same(name)));
			will(returnValue(true));
			oneOf(repository).getTag(with(same(name)));
		} });
		
		service.getTag(name);
	}
	
	@Test
	public void testGetTagDoesntExist() {
		context.checking(new Expectations() { { 
			oneOf(repository).tagExists(with(same(name)));
			will(returnValue(false));
			oneOf(repository).newTag(with(same(name)));
		} });
		
		service.getTag(name);
	}
	
	@Test
	public void testTagSearch() {
		final List<Tag> tags = new ArrayList<Tag>();
		context.checking(new Expectations() { {
			oneOf(repository).search(with(same(name)));
			will(returnValue(tags));
		} });
		
		service.search(name);
	}
	
	@Test
	public void deleteTag() {
		context.checking(new Expectations() { {
			oneOf(repository).delete(with(same(name)));
		} });
		
		service.deleteTag(name);
	}
	
}
