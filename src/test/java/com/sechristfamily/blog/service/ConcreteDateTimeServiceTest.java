package com.sechristfamily.blog.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class ConcreteDateTimeServiceTest {

	private ConcreteDateTimeService service = new ConcreteDateTimeService();
	
	@Test
	public void testGetCurrentDateSuccess() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			// Do nothing
		}
		
		assertThat(service.getCurrentDate(), is(equalTo(date)));
	}
}
