package com.sechristfamily.blog.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.sechristfamily.blog.domain.TagEntity;
import com.sechristfamily.blog.tests.Deployments;

@RunWith(Arquillian.class)
public class JpaTagRepositoryIT {

	@Deployment
	public static Archive<?> createDeployment() {
		return Deployments.createFullDeployment(Deployments.ALWAYS_LOGGED_IN_BEAN);
	}
	
	@Inject
	private TagRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Inject
	private UserTransaction tx;
	
	@Before
	public void setUp() throws Exception {
		tx.begin();
		entityManager.joinTransaction();
	}
	
	@After
	public void tearDown() throws Exception {
		tx.rollback();
	}
	
	@Test
	public void testAddTagSuccess() throws Exception {
		repository.newTag("Test");
		
		entityManager.flush();
		entityManager.clear();
		
		TagEntity tag = new TagEntity();
		try {
			tag = (TagEntity) entityManager
				.createNamedQuery("findTagByName")
				.setParameter("tag", "Test")
				.getSingleResult();
		} catch (NoResultException e) {
			tag = null;
		}
		
		assertThat(tag, is(not(nullValue())));
		assertThat(tag.getName(), is(equalTo("Test")));
	}
	
}
