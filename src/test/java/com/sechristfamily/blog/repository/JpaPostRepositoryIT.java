package com.sechristfamily.blog.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.domain.PostEntity;
import com.sechristfamily.blog.tests.Deployments;

@RunWith(Arquillian.class)
public class JpaPostRepositoryIT {

	@Deployment
	public static Archive<?> createDeployment() {
		return Deployments.createFullDeployment(Deployments.ALWAYS_LOGGED_IN_BEAN);
	}
	
	@Inject
	private PostRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Inject
	private UserTransaction tx;
	
	@Before
	public void setUp() throws Exception {
	  tx.begin();
	  entityManager.joinTransaction();
	}
	  
	@After
	public void tearDown() throws Exception {
	  tx.rollback();
	}
	  
	@Test
	public void testAddSuccess() throws Exception {
		PostEntity post = new PostEntity();
		post.setTitle("Sample Title");
		post.setText("A blog post about who knows what! It's so exciting! Here is a picture <img src='whatev.jpg' />");
		post.setDatePosted((new SimpleDateFormat("yyyy-MM-dd")).parse((new SimpleDateFormat("yyyy-MM-dd").format(new Date()))));
		
		repository.add(post);
		
		entityManager.flush();
		entityManager.clear();
		
		PostEntity actual = entityManager.find(PostEntity.class, post.getId());
		
		assertThat(actual, is(not(nullValue())));
		assertThat(actual, is(not(sameInstance(post))));
		assertThat(actual.getTitle(), is(equalTo(post.getTitle())));
		assertThat(actual.getText(), is(equalTo(post.getText())));
	}
	
	@Test(expected = PersistenceException.class)
	public void testDuplicateTitle() throws Exception {
		PostEntity post1 = new PostEntity();
		PostEntity post2 = new PostEntity();
		post1.setTitle("Title");
		post2.setTitle("Title");
		post1.setText("Here is Text");
		post2.setText("Here is Text");
		
		repository.add(post1);
		
		entityManager.flush();
		entityManager.clear();
		
		repository.add(post2);
		
		entityManager.flush();
		entityManager.clear();
	}
	
	@Test
	public void testRetrieveAllSuccess() {
		List<Post> posts = new ArrayList<Post>();
		
		List<Post> actual = repository.getAll();
		
		assertThat(actual, is(not(nullValue())));
		assertThat(actual, is(equalTo(posts)));
	}
}
