package com.sechristfamily.blog.facelets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.jmock.Expectations.returnValue;
import static org.jmock.Expectations.throwException;

import org.jmock.Expectations;
import org.jmock.api.Action;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.service.AddPostException;
import com.sechristfamily.blog.service.AddPostService;

public class AddPostBeanTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();
	
	@Mock
	private Post post;
	
	@Mock
	private AddPostService service;
	
	private AddPostBean bean = new AddPostBean();
	
	@Before
	public void setup() {
		bean.postService = service;
		bean.setPost(post);
	}
	
	@Test
	public void testSaveSuccess() throws Exception {
		context.checking(savePostExpectations(returnValue(null)));
		
		assertThat(bean.save(), is(equalTo(AddPostBean.SUCCESS_ID)));
	}
	
	@Test
	public void testSaveFailure() throws Exception {
		context.checking(savePostExpectations(throwException(new AddPostException())));
		
		assertThat(bean.save(), is(nullValue()));
	}
	
	private Expectations savePostExpectations(final Action outcome) throws Exception {
		return new Expectations() { { 
			oneOf(service).savePost(with(same(post)));
			will(outcome);
		} };
	}
	
	@Test
	public void testPublishSuccess() throws Exception {
		context.checking(publishPostExpectations(returnValue(null)));
		
		assertThat(bean.publish(), is(equalTo(AddPostBean.SUCCESS_ID)));
	}
	
	@Test
	public void testPublishFailure() throws Exception {
		context.checking(publishPostExpectations(throwException(new AddPostException())));
		
		assertThat(bean.publish(), is(nullValue()));
	}

	private Expectations publishPostExpectations(final Action outcome) throws Exception {
		return new Expectations() { {
			oneOf(service).publishPost(with(same(post)));
			will(outcome);
		} };
	}
	
}
