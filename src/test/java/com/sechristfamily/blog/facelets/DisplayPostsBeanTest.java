package com.sechristfamily.blog.facelets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.service.RetrievePostsService;

public class DisplayPostsBeanTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery() {{
	  setImposteriser(ClassImposteriser.INSTANCE);	
	}};
	
	@Mock
	private RetrievePostsService service;
	
	private DisplayPostsBean bean = new DisplayPostsBean();
	
	@Before
	public void setUp() {
		bean.service = service;
	}
	
	@Test
	public void testRetrievePosts() {
		final List<Post> posts = new ArrayList<Post>();
		context.checking(new Expectations() { {
			oneOf(service).getPosts();
			will(returnValue(posts));
		} });
		
		bean.init();
		assertThat(bean.getPosts(), is(equalTo(posts)));
	}
	
	@Test
	public void testSearch() {
		final List<Post> posts = new ArrayList<Post>();
		final String query = "Test";
		context.checking(new Expectations() { {
			oneOf(service).search(query);
			will(returnValue(posts));
		} });
		
		bean.setQuery(query);
		bean.search();
		assertEquals(posts, bean.getPosts());
	}
}
