package com.sechristfamily.blog.facelets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;

import org.jmock.auto.Mock;
import org.junit.Test;

import com.sechristfamily.blog.Post;

public class DisplayPostBeanTest {

	@Mock
	private Post post;
	
	private DisplayPostBean bean = new DisplayPostBean();
	
	@Test
	public void testSetPostSuccess() {
		bean.setPost(post);
		
		assertThat(bean.getPost(), is(sameInstance(post)));
	}
}
