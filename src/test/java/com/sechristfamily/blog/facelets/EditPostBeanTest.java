package com.sechristfamily.blog.facelets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.service.UpdatePostException;
import com.sechristfamily.blog.service.UpdatePostService;

public class EditPostBeanTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();
	
	@Mock
	private Post updatedPost;
	
	@Mock
	private UpdatePostService service;
	
	private EditPostBean bean = new EditPostBean();
	
	@Before
	public void setup() {
		bean.service = service;
		
		bean.updatedPost = updatedPost;

		bean.setPost(updatedPost);
	}
	
	@Test
	public void testUpdateSuccess() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).updatePost(with(same(updatedPost)));
			will(returnValue(null));
		} });
		
		assertThat(bean.update(), is(equalTo(AddPostBean.SUCCESS_ID)));
	}
	
	@Test
	public void testUpdateFailure() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).updatePost(with(same(updatedPost)));
			will(throwException(new UpdatePostException()));
		} });
		
		assertThat(bean.update(), is(nullValue()));
	}
	
	@Test
	public void testSaveSuccess() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).savePost(with(same(updatedPost)));
			will(returnValue(null));
		} });
		
		assertThat(bean.save(), is(equalTo(AddPostBean.SUCCESS_ID)));
	}
	
	@Test
	public void testSaveFailure() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).savePost(with(same(updatedPost)));
			will(throwException(new UpdatePostException()));
		} });
		
		assertThat(bean.save(), is(nullValue()));
	}
	
	@Test
	public void testPublishSuccess() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).publishPost(with(same(updatedPost)));
			will(returnValue(null));
		} });
		
		assertThat(bean.publish(), is(equalTo(AddPostBean.SUCCESS_ID)));
	}
	
	@Test
	public void testPublishFailure() throws Exception {
		context.checking(new Expectations() { {
			oneOf(service).publishPost(with(same(updatedPost)));
			will(throwException(new UpdatePostException()));
		} });
		
		assertThat(bean.publish(), is(nullValue()));
	}
	
}
