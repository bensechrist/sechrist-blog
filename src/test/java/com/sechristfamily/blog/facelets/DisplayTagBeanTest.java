package com.sechristfamily.blog.facelets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sechristfamily.blog.Post;
import com.sechristfamily.blog.service.RetrievePostsService;

public class DisplayTagBeanTest {

	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();
	
	@Mock
	private RetrievePostsService service;
	
	private DisplayTagBean bean = new DisplayTagBean();
	
	@Before
	public void init() {
		bean.service = service;
	}
	
	@Test
	public void testGetPostsFromTagSuccess() {
		final String tagName = "Test";
		final List<Post> posts = new ArrayList<Post>();
		context.checking(new Expectations() { { 
			oneOf(service).getPostsByTag(tagName);
			will(returnValue(posts));
		} });

		bean.setTagName(tagName);
		assertThat(bean.getPosts(), is(equalTo(posts)));
	}
}
