# Sechrist Blog

This is a blog I made for my wife using:
-   [Java EE 7](http://www.oracle.com/technetwork/java/javaee/overview/index.html)
-   [Bootstrap](http://www.getbootstrap.com)
-   [jQuery](http://jquery.com/)
-   [JPA](http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html)
-   [Keycloak](http://keycloak.jboss.org/)

The testing was done using:
-   [JUnit](http://junit.org/)
-   [JMock](http://jmock.org/)
-   [Arquillian](http://arquillian.org/)